import pandas as pd
import os, sys, random
from sklearn.model_selection import train_test_split

if "research" in os.getcwd():
    sys.path.append('../')
    os.environ["GLOBAL_PARENT_DIR"] = ".."

else:
    os.environ["GLOBAL_PARENT_DIR"] = ".."

# p is set to a value between 0.0 and 1.0, so we an read in a percentage `p` sized portion of the data
p = 1.0


def read_in_data(file):
    return pd.read_csv(file,
                       sep='\t',
                       header=None,
                       names=['label', 'comment', 'author', 'subreddit', 'score',
                              'ups', 'downs', 'date', 'created_utc', 'parent_comment'],
                       usecols=['label', 'author', 'subreddit', 'score',
                                'date', 'created_utc', 'parent_comment', 'comment'],  # ignore ups and downs cols
                       skiprows=lambda i: i > 0 and random.random() > p)

# ===============================================================
# Train Data
# ===============================================================


data_train = read_in_data("resources/train-balanced.csv")
data_train = data_train[['label', 'author', 'subreddit', 'score', 'parent_comment', 'comment']]

data_train.head()
data_train.dropna(inplace=True)

# Test Data

data_test = read_in_data("resources/test-balanced.csv")
data_test = data_test[['label', 'author', 'subreddit', 'score', 'parent_comment', 'comment']]

data_test.head()
data_test.dropna(inplace=True)

# Concatenate all together
data_sarcasm = pd.concat([data_train, data_test], axis=0, sort=False)
# Keep only those columns which are more relevant

relevant_cols = ["label", "comment"]

data_sarcasm = data_sarcasm[relevant_cols]

# Rename columns for flair
data_sarcasm.rename(columns={"comment": "text",
                             "label": "label_sarcastic"},
                    inplace=True)

# Generate Datasets for training/testing and dev
test_size = .2
dev_size = .3

label = "label_sarcastic"

df_train, df_test = train_test_split(data_sarcasm, test_size=test_size, stratify=data_sarcasm[label])

# Split test dev
df_train, df_dev = train_test_split(df_train, test_size=dev_size, stratify=df_train[label])

train_filename = os.path.join("data", "train.csv")
test_filename = os.path.join("data", "test.csv")
dev_filename = os.path.join("data",  "dev.csv")

# Print Shapes
print(df_train.shape)
print(df_dev.shape)
print(df_test.shape)

df_train.to_csv(train_filename, index=False)
df_test.to_csv(test_filename, index=False)
df_dev.to_csv(dev_filename, index=False)